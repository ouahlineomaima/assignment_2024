package ma.nemo.assignment.serviceImp;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.domain.util.EventType;
import ma.nemo.assignment.dto.SupplyDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.mapper.ProductMapper;
import ma.nemo.assignment.mapper.SupplyMapper;
import ma.nemo.assignment.mapper.TransactionHistoryMapper;
import ma.nemo.assignment.repository.SupplyRepository;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.service.SupplyService;
import ma.nemo.assignment.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class SupplyServiceImp implements SupplyService {
    private final SupplyRepository supplyRepository;
    private final SupplyMapper supplyMapper;
    private final ProductService productService;
    private final ProductMapper productMapper;
    private final TransactionHistoryService transactionHistoryService;
    private final TransactionHistoryMapper transactionHistoryMapper;
    @Transactional
    @Override
    public SupplyDto createSupply(SupplyDto supplyDto) throws ProductNotFound{
        Product product = productService.getProductByCode(supplyDto.getProductCode());
        Supply supply = new Supply();
        supply.setProduct(product);
        supply.setQuantity(supplyDto.getQuantity());
        supply.setExpirationDate(supplyDto.getExpirationDate());
        LocalDateTime supplyDate = LocalDateTime.now();
        supply.setSupplyDate(supplyDate);

        supplyRepository.save(supply);


        Integer newQuantity = supplyDto.getQuantity() + product.getQuantityInStock();
        product.setQuantityInStock(newQuantity);
        product.setModificationDate(supplyDate);
        product.setExpirationDate(supplyDto.getExpirationDate());
        productService.updateProduct(productMapper.toDTO(product));

        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setProduct(product);
        transactionHistory.setQuantity(supplyDto.getQuantity());
        transactionHistory.setTransactionType(EventType.SUPPLY);
        transactionHistory.setTransactionDate(supplyDate);
        transactionHistory.setUser(null);

        transactionHistoryService.createTransactionHistory(transactionHistoryMapper.toDTO(transactionHistory));


        return supplyMapper.toDTO(supply);
    }
}
