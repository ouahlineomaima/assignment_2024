package ma.nemo.assignment.serviceImp;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.ReturnedProduct;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.domain.util.EventType;
import ma.nemo.assignment.dto.ReturnedProductDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.mapper.ProductMapper;
import ma.nemo.assignment.mapper.ReturnedProductMapper;
import ma.nemo.assignment.mapper.TransactionHistoryMapper;
import ma.nemo.assignment.repository.ReturnedProductRepository;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.service.ReturnedProductService;
import ma.nemo.assignment.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class ReturnedProductServiceImp implements ReturnedProductService {
    private final ReturnedProductRepository returnedProductRepository;
    private final ReturnedProductMapper returnedProductMapper;

    private final TransactionHistoryService transactionHistoryService;
    private final TransactionHistoryMapper transactionHistoryMapper;

    private final ProductService productService;
    private final ProductMapper productMapper;
    @Transactional
    @Override
    public ReturnedProductDto createReturnedProduct(ReturnedProductDto returnedProductDto) throws ProductNotFound{
        Product product = productService.getProductByCode(returnedProductDto.getProductCode());
        if(product == null){
            throw new ProductNotFound("Product with code : " + returnedProductDto.getProductCode() + " not found");
        }

        ReturnedProduct returnedProduct = new ReturnedProduct();
        LocalDateTime returnDate = LocalDateTime.now();
        returnedProduct.setProduct(product);
        returnedProduct.setReturnDate(returnDate);
        returnedProduct.setUser(null);
        returnedProduct.setReason(returnedProductDto.getReason());
        returnedProduct.setReturnedQuantity(returnedProductDto.getQuantity());

        returnedProductRepository.save(returnedProduct);

        Integer newQuantity = returnedProductDto.getQuantity() + product.getQuantityInStock();
        product.setQuantityInStock(newQuantity);
        product.setModificationDate(returnDate);

        productService.updateProduct(productMapper.toDTO(product));

        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setProduct(product);
        transactionHistory.setQuantity(returnedProductDto.getQuantity());
        transactionHistory.setTransactionType(EventType.RETURN);
        transactionHistory.setTransactionDate(returnDate);
        transactionHistory.setUser(null);

        transactionHistoryService.createTransactionHistory(transactionHistoryMapper.toDTO(transactionHistory));

        return returnedProductMapper.toDTO(returnedProduct);
    }
}
