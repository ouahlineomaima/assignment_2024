package ma.nemo.assignment.serviceImp;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.dto.TransactionHistoryDto;
import ma.nemo.assignment.mapper.TransactionHistoryMapper;
import ma.nemo.assignment.repository.TransactionHistoryRepository;
import ma.nemo.assignment.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class TransactionHistorySeviceImp implements TransactionHistoryService {
    private final TransactionHistoryMapper transactionHistoryMapper;
    private final TransactionHistoryRepository transactionHistoryRepository;
    @Transactional
    @Override
    public TransactionHistoryDto createTransactionHistory(TransactionHistoryDto transactionHistoryDto) {
        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setTransactionDate(LocalDateTime.now());
        transactionHistory.setTransactionType(transactionHistoryDto.getTransactionType());
        transactionHistory.setUser(transactionHistoryDto.getUser());
        transactionHistory.setProduct(transactionHistoryDto.getProduct());
        transactionHistory.setQuantity(transactionHistoryDto.getQuantity());

        transactionHistoryRepository.save(transactionHistory);

        return transactionHistoryMapper.toDTO(transactionHistory);
    }
}
