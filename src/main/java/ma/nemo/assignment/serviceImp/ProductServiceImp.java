package ma.nemo.assignment.serviceImp;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.dto.ThresholdQuantityDto;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.mapper.ProductMapper;
import ma.nemo.assignment.repository.ProductRepository;
import ma.nemo.assignment.service.ProductService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    @Transactional
    @Override
    public ProductDto createProduct(ProductDto productDto) throws ProductAlreadyExists{
        Optional<Product> product = productRepository.findByProductCode(productDto.getProductCode());
        if(product.isPresent()){
            throw new ProductAlreadyExists("Product with code :" + productDto.getProductCode()+" already exist");
        }
        Product newProduct = new Product();
        newProduct.setProductCode(productDto.getProductCode());
        newProduct.setProductName(productDto.getProductName());
        newProduct.setDescription(productDto.getDescription());
        newProduct.setQuantityInStock(productDto.getQuantityInStock());
        newProduct.setUnitPrice(productDto.getUnitPrice());

        LocalDateTime currentDateTime= LocalDateTime.now();
        newProduct.setCreationDate(currentDateTime);
        newProduct.setModificationDate(currentDateTime);
        newProduct.setExpirationDate(productDto.getExpirationDate());

        productRepository.save(newProduct);
        return productMapper.toDTO(newProduct);
    }

    @Override
    public List<ProductDto> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(productMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto getProductById(Long id) throws ProductNotFound {
        Optional<Product> product = productRepository.findById(id);
        if(product.isPresent()){
            ProductDto productDto = productMapper.toDTO(product.get());
            return productDto;
        }
        throw new ProductNotFound("Product with id " + id + " not found");
    }

    @Override
    public Product getProductByCode(String productCode) throws ProductNotFound {
        Optional<Product> product = productRepository.findByProductCode(productCode);
        if(product.isPresent()){
            return product.get();
        }
        throw new ProductNotFound("Product with id " + productCode + " not found");
    }

    @Override
    public boolean deleteProductById(Long id) throws ProductNotFound {
        Optional<Product> productToDelete = productRepository.findById(id);
        if(productToDelete.isPresent()){
            productRepository.deleteById(id);
            return true;
        }
        else {
            throw new ProductNotFound("Product with id "+ id +" Doesn't exist");
        }
    }
    @Transactional
    @Override
    public ProductDto updateProduct(ProductDto productDto) throws ProductNotFound {
        Optional<Product> existingProduct = productRepository.findByProductCode(productDto.getProductCode());

        if (existingProduct.isEmpty()) {
            throw new ProductNotFound("Product not found with code: " + productDto.getProductCode());
        }

        existingProduct.get().setProductName(productDto.getProductName());
        existingProduct.get().setDescription(productDto.getDescription());
        existingProduct.get().setUnitPrice(productDto.getUnitPrice());
        existingProduct.get().setQuantityInStock(productDto.getQuantityInStock());
        existingProduct.get().setQuantityThreshold(productDto.getQuantityThreshold());
        existingProduct.get().setExpirationDate(productDto.getExpirationDate());

        Product savedProduct = productRepository.save(existingProduct.get());
        return productMapper.toDTO(savedProduct);
    }


    @Override
    public List<ProductDto> getProductsNearExpiryDate(int thresholdDays){
        LocalDate currentDate = LocalDate.now();
        LocalDateTime thresholdStartDate = currentDate.atStartOfDay();
        LocalDateTime thresholdEndDate = currentDate.plusDays(thresholdDays).atTime(23, 59, 59);

        List<Product> products = productRepository.findByExpirationDateBetween(thresholdStartDate, thresholdEndDate);
        if (products.isEmpty()){
            return null;
        }

        return products.stream()
                .map(productMapper::toDTO)
                .collect(Collectors.toList());
    }
    @Transactional
    @Override
    public void setThresholdQuantity(ThresholdQuantityDto thresholdQuantityDto) throws ProductNotFound {
        if(productRepository.findByProductCode(thresholdQuantityDto.getProductCode()).isPresent()){
            Product product = productRepository.findByProductCode(thresholdQuantityDto.getProductCode()).get();
            product.setQuantityThreshold(thresholdQuantityDto.getThresholdQuantity());
            productRepository.save(product);
        }
        else{
            throw new ProductNotFound("Product with code " + thresholdQuantityDto.getProductCode() +" not found");
        }

    }

    @Override
    public List<ProductDto> getAllProductsBellowQuantityThreshold(){
        List<Product> products = productRepository.findProductBelowQuantityThreshold();
        if(products.isEmpty()){
            return null;
        }
        else{
            return products.stream()
                    .map(productMapper::toDTO)
                    .collect(Collectors.toList());
        }
    }
}
