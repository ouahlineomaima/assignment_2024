package ma.nemo.assignment.serviceImp;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.domain.Sale;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.domain.util.EventType;
import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.QuantityUnavailableException;
import ma.nemo.assignment.mapper.ProductMapper;
import ma.nemo.assignment.mapper.SaleMapper;
import ma.nemo.assignment.mapper.TransactionHistoryMapper;
import ma.nemo.assignment.repository.SaleRepository;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.service.SaleService;
import ma.nemo.assignment.service.TransactionHistoryService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class SaleServiceImp implements SaleService {
    private final SaleRepository saleRepository;
    private final ProductService productService;
    private final SaleMapper saleMapper;
    private final ProductMapper productMapper;
    private final TransactionHistoryService transactionHistoryService;
    private final TransactionHistoryMapper transactionHistoryMapper;
    @Transactional
    @Override
    public SaleDto createSale(SaleDto saleDto) throws ProductNotFound, QuantityUnavailableException{
        Product product = productService.getProductByCode(saleDto.getProductCode());
        Integer saleQuantity = saleDto.getQuantity();
        if(saleQuantity > product.getQuantityInStock()){
            throw new QuantityUnavailableException("Quantité indisponible dans le stock");
        }
        Sale savedSale = new Sale();
        savedSale.setProduct(product);
        savedSale.setUser(null);
        savedSale.setSoldQuantity(saleQuantity);
        savedSale.setTotalPrice(product.getUnitPrice().multiply(BigDecimal.valueOf(saleQuantity)));
        LocalDateTime saleDate = LocalDateTime.now();
        savedSale.setSaleDate(saleDate);
        saleRepository.save(savedSale);

        product.setModificationDate(saleDate);
        product.setQuantityInStock(product.getQuantityInStock() - saleQuantity);
        productService.updateProduct(productMapper.toDTO(product));

        TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setProduct(product);
        transactionHistory.setQuantity(saleDto.getQuantity());
        transactionHistory.setTransactionType(EventType.SALE);
        transactionHistory.setTransactionDate(saleDate);
        transactionHistory.setUser(null);

        transactionHistoryService.createTransactionHistory(transactionHistoryMapper.toDTO(transactionHistory));

        return saleMapper.toDTO(savedSale);
    }
}
