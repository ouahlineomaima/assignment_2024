package ma.nemo.assignment.exceptions;

public class QuantityUnavailableException extends Exception{
    private static final long serialVersionUID = 1L;
    public QuantityUnavailableException(){}

    public QuantityUnavailableException(String message){
        super(message);
    }
}
