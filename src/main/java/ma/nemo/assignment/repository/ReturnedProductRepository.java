package ma.nemo.assignment.repository;

import ma.nemo.assignment.domain.ReturnedProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReturnedProductRepository extends JpaRepository<ReturnedProduct, Long> {
}
