package ma.nemo.assignment.web;

import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/expiry-alerts")
@RequiredArgsConstructor
public class ProductExpiryAlertController {

    private final ProductService productService;
    Logger LOGGER = LoggerFactory.getLogger(ProductExpiryAlertController.class);

    @GetMapping("/{thresholdDays}")
    public ResponseEntity<List<ProductDto>> getProductNearExpiryDate(@PathVariable int thresholdDays){
        LOGGER.info("Getting product near expiration date :");

        return new ResponseEntity<List<ProductDto>>(productService.getProductsNearExpiryDate(thresholdDays), HttpStatus.OK);


    }
}
