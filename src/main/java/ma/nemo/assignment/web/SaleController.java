package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.QuantityUnavailableException;
import ma.nemo.assignment.service.SaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sale")
@RequiredArgsConstructor
public class SaleController {

    Logger LOGGER = LoggerFactory.getLogger(SaleController.class);

    private final SaleService saleService;

    @PostMapping
    public ResponseEntity<SaleDto> registerSoldProduct(@Valid  @RequestBody SaleDto saleDto) throws ProductNotFound, QuantityUnavailableException{
        LOGGER.info("New sale for product: " + saleDto.getProductCode());
        return new ResponseEntity<SaleDto>(saleService.createSale(saleDto), HttpStatus.CREATED);
    }
}
