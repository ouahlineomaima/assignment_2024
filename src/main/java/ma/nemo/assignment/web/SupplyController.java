package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.dto.SupplyDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.service.SupplyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/supply")
@RequiredArgsConstructor
public class SupplyController {
    Logger LOGGER = LoggerFactory.getLogger(SupplyController.class);

    private final SupplyService supplyService;

    @PostMapping
    public ResponseEntity<SupplyDto> addProductToInventory(@Valid  @RequestBody SupplyDto supplyDto) throws ProductNotFound{
        SupplyDto savedSupply = supplyService.createSupply(supplyDto);
        LOGGER.info("Creating new Supply: {}", savedSupply);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedSupply);
    }
}
