package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.dto.ReturnedProductDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.service.ReturnedProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/return")
@RequiredArgsConstructor
public class ReturnedProductController {
    private final ReturnedProductService returnedProductService;

    Logger LOGGER = LoggerFactory.getLogger(ReturnedProductController.class);

    @PostMapping
    public ResponseEntity<ReturnedProductDto> registerReturnedProduct(@Valid  @RequestBody ReturnedProductDto returnedProductDto) throws ProductNotFound {
        LOGGER.info("Registering returned product : " + returnedProductDto.getProductCode());

        return new ResponseEntity<ReturnedProductDto>(returnedProductService.createReturnedProduct(returnedProductDto), HttpStatus.CREATED);
    }
}
