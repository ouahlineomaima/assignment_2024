package ma.nemo.assignment.web;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.dto.ThresholdQuantityDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductQuantityThresholdController {

    private final ProductService productService;
    Logger LOGGER = LoggerFactory.getLogger(ProductQuantityThresholdController.class);

    @GetMapping("/api/threshold-alerts")
    public ResponseEntity<List<ProductDto>> getAllProductBelowQuantityThreshold(){
        LOGGER.info("Getting list of product below quantity threshold");
        return new ResponseEntity<List<ProductDto>>(productService.getAllProductsBellowQuantityThreshold(), HttpStatus.OK);
    }

    @PostMapping("/api/set-threshold")
    public ResponseEntity<?> setQuantityThreshold(@Valid @RequestBody ThresholdQuantityDto thresholdQuantityDto) throws ProductNotFound {
        LOGGER.info("Set the quantity threshold of product: "+thresholdQuantityDto.getProductCode());

        productService.setThresholdQuantity(thresholdQuantityDto);
        return new ResponseEntity<>("Quantity threshold set successfully", HttpStatus.OK);
    }

}
