package ma.nemo.assignment.service;

import ma.nemo.assignment.dto.ReturnedProductDto;
import ma.nemo.assignment.exceptions.ProductNotFound;


public interface ReturnedProductService {

    ReturnedProductDto createReturnedProduct(ReturnedProductDto returnedProductDto) throws ProductNotFound;
}
