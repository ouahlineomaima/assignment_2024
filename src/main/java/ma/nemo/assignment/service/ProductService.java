package ma.nemo.assignment.service;

import ma.nemo.assignment.domain.Product;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.dto.ThresholdQuantityDto;
import ma.nemo.assignment.exceptions.ProductAlreadyExists;
import ma.nemo.assignment.exceptions.ProductNotFound;


import java.util.List;


public interface ProductService {
    ProductDto createProduct(ProductDto productDto) throws ProductAlreadyExists;

    List<ProductDto> getAllProducts();

    ProductDto getProductById(Long id) throws ProductNotFound;

    Product getProductByCode(String productCode) throws ProductNotFound;

    boolean deleteProductById(Long id) throws ProductNotFound;

    ProductDto updateProduct(ProductDto productDto) throws ProductNotFound;


    List<ProductDto> getProductsNearExpiryDate(int thresholdDays);

    void setThresholdQuantity(ThresholdQuantityDto thresholdQuantityDto) throws ProductNotFound;

    List<ProductDto> getAllProductsBellowQuantityThreshold();
}
