package ma.nemo.assignment.service;

import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.exceptions.ProductNotFound;
import ma.nemo.assignment.exceptions.QuantityUnavailableException;



public interface SaleService {
    SaleDto createSale(SaleDto saleDto) throws ProductNotFound, QuantityUnavailableException;
}
