package ma.nemo.assignment.service;

import ma.nemo.assignment.dto.TransactionHistoryDto;


public interface TransactionHistoryService {
    TransactionHistoryDto createTransactionHistory(TransactionHistoryDto transactionHistoryDto);
}
