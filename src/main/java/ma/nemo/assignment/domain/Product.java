package ma.nemo.assignment.domain;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "Products")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long productId;

  @Column(unique = true, nullable = false)
  private String productCode;

  @Column(nullable = false)
  private String productName;

  private String description;

  private BigDecimal unitPrice;

  private Integer quantityInStock;

  @Column(columnDefinition = "TIMESTAMP")
  private LocalDateTime creationDate;

  @Column(columnDefinition = "TIMESTAMP")
  private LocalDateTime modificationDate;

  @Column(columnDefinition = "TIMESTAMP")
  private LocalDateTime expirationDate;


  private Integer quantityThreshold;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ReturnedProduct> returns;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Supply> supplies;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Sale> sales;

  @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<TransactionHistory> transactions;


  // Constructor, Getters & setters are added by lombok at compile time.
}