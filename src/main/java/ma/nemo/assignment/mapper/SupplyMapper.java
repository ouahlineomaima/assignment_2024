package ma.nemo.assignment.mapper;

import ma.nemo.assignment.domain.Supply;
import ma.nemo.assignment.dto.SupplyDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class SupplyMapper {
    private final ModelMapper modelMapper;

    public SupplyMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public SupplyDto toDTO(Supply supply) {
        return modelMapper.map(supply, SupplyDto.class);
    }

    public Supply toEntity(SupplyDto supplyDto) {
        return modelMapper.map(supplyDto, Supply.class);
    }
}
