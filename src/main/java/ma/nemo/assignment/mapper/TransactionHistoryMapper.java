package ma.nemo.assignment.mapper;

import lombok.AllArgsConstructor;
import ma.nemo.assignment.domain.TransactionHistory;
import ma.nemo.assignment.dto.TransactionHistoryDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class TransactionHistoryMapper {
    private final ModelMapper modelMapper;

    public TransactionHistoryDto toDTO(TransactionHistory transactionHistory){
       return  modelMapper.map(transactionHistory, TransactionHistoryDto.class);
    }

    public TransactionHistory toEntity(TransactionHistoryDto transactionHistoryDto){
       return  modelMapper.map(transactionHistoryDto, TransactionHistory.class);
    }




}
