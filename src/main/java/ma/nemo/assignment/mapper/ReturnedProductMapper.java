package ma.nemo.assignment.mapper;

import lombok.RequiredArgsConstructor;
import ma.nemo.assignment.domain.ReturnedProduct;
import ma.nemo.assignment.dto.ReturnedProductDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ReturnedProductMapper {

    private final ModelMapper modelMapper;

    public ReturnedProductDto toDTO(ReturnedProduct returnedProduct){
        return modelMapper.map(returnedProduct, ReturnedProductDto.class);
    }

    public ReturnedProduct toEntity(ReturnedProductDto returnedProductDto){
        return modelMapper.map(returnedProductDto, ReturnedProduct.class);
    }
}
