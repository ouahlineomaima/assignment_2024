package ma.nemo.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.dto.SaleDto;
import ma.nemo.assignment.service.SaleService;
import ma.nemo.assignment.web.SaleController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class SaleControllerTest {

    private MockMvc mockMvc;

    @Mock
    private SaleService saleService;

    @InjectMocks
    private SaleController saleController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(saleController).build();
    }
    @Test
    public void testAddSale() throws Exception {
        SaleDto saleDTO = new SaleDto();
        saleDTO.setProductCode("TEST1");
        saleDTO.setQuantity(5);

        when(saleService.createSale(saleDTO)).thenReturn(saleDTO);

        ObjectMapper objectMapper = new ObjectMapper();
        String saleJson = objectMapper.writeValueAsString(saleDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/sale")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(saleJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}