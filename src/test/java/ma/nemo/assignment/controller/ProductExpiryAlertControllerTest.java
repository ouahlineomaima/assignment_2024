package ma.nemo.assignment.controller;

import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.web.ProductExpiryAlertController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class ProductExpiryAlertControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductExpiryAlertController productExpiryAlertController;

    @Test
    public void testGetProductNearExpiryDate() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productExpiryAlertController).build();
        int thresholdDays = 5;

        List<ProductDto> productDtos = new ArrayList<>();
        // Add some sample ProductDto to the list
        ProductDto product1 = new ProductDto("P001", "Test Product", "Sample product for testing", BigDecimal.valueOf(100), 10, 5, LocalDateTime.of(2023, 11, 6, 2, 2));
        productDtos.add(product1);

        ProductDto product2 = new ProductDto("P002", "Test Product", "Sample product for testing", BigDecimal.valueOf(100), 10, 5, LocalDateTime.of(2024, 2, 2, 2, 2));
        productDtos.add(product2);

        when(productService.getProductsNearExpiryDate(thresholdDays)).thenReturn(Collections.singletonList(product1));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/expiry-alerts/{thresholdDays}", thresholdDays)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].productCode", Matchers.is("P001")));
    }
}
