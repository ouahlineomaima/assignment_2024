package ma.nemo.assignment.controller;

import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.dto.ThresholdQuantityDto;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.web.ProductQuantityThresholdController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductQuantityThresholdControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductQuantityThresholdController productQuantityThresholdController;

    @Test
    public void testGetAllProductBelowQuantityThreshold() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productQuantityThresholdController).build();

        List<ProductDto> productDtos = new ArrayList<>();

        // Add some sample ProductDto to the list
        ProductDto product1 = new ProductDto("P001", "Product 1", "Sample product 1 for testing", BigDecimal.valueOf(100), 3, 5, LocalDateTime.now());
        productDtos.add(product1);

        ProductDto product2 = new ProductDto("P002", "Product 2", "Sample product 2 for testing", BigDecimal.valueOf(100), 8, 5, LocalDateTime.now());
        productDtos.add(product2);

        when(productService.getAllProductsBellowQuantityThreshold()).thenReturn(productDtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/threshold-alerts")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].productCode", Matchers.is("P001")));
    }


    @Test
    public void testSetQuantityThreshold() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productQuantityThresholdController).build();

        ThresholdQuantityDto thresholdQuantityDto = new ThresholdQuantityDto();
        thresholdQuantityDto.setProductCode("product1");
        thresholdQuantityDto.setThresholdQuantity(5);

        doNothing().when(productService).setThresholdQuantity(thresholdQuantityDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/set-threshold")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(thresholdQuantityDto.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}