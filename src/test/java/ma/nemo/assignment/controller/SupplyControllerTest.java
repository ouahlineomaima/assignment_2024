package ma.nemo.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.config.SupplyServiceTestConfig;
import ma.nemo.assignment.dto.SupplyDto;
import ma.nemo.assignment.service.SupplyService;
import ma.nemo.assignment.web.SupplyController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SupplyController.class)
@Import(SupplyServiceTestConfig.class)
public class SupplyControllerTest {
    private final Logger LOGGER = LoggerFactory.getLogger(SupplyControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private SupplyService supplyService;

    @InjectMocks
    private SupplyController supplyController;

    @Autowired
    private ObjectMapper objectMapper;
    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(supplyController).build();
    }

    @Test
    public void testAddProductToInventory() throws Exception {
        LocalDateTime expirationDate = LocalDateTime.now();
        SupplyDto supplyDto = new SupplyDto("P001", 100, expirationDate);

        when(supplyService.createSupply(supplyDto)).thenReturn(supplyDto);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/supply")
                        .content(objectMapper.writeValueAsString(supplyDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn();
    }
}
