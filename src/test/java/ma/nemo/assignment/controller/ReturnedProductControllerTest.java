package ma.nemo.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.dto.ReturnedProductDto;
import ma.nemo.assignment.service.ReturnedProductService;
import ma.nemo.assignment.web.ReturnedProductController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReturnedProductControllerTest {

    @Mock
    private ReturnedProductService returnedProductService;

    @InjectMocks
    private ReturnedProductController returnedProductController;

    @Test
    public void testRegisterReturnedProduct() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(returnedProductController).build();

        ReturnedProductDto returnedProductDto = new ReturnedProductDto();
        returnedProductDto.setProductCode("P001");
        returnedProductDto.setQuantity(5);
        returnedProductDto.setReason("Defective product");

        when(returnedProductService.createReturnedProduct(returnedProductDto)).thenReturn(returnedProductDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/return")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(returnedProductDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(new ObjectMapper().writeValueAsString(returnedProductDto)));
    }

}
