package ma.nemo.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.nemo.assignment.dto.ProductDto;
import ma.nemo.assignment.service.ProductService;
import ma.nemo.assignment.web.ProductController;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    @Test
    public void testCreateProduct() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        ProductDto productDto = new ProductDto();
        productDto.setProductCode("P009");
        productDto.setProductName("Test Product");
        productDto.setDescription("Sample product for testing");
        productDto.setQuantityInStock(10);
        productDto.setQuantityThreshold(5);
        productDto.setUnitPrice(BigDecimal.valueOf(100.0));

        when(productService.createProduct(productDto)).thenReturn(productDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(productDto)))  // Convert the DTO to JSON
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productCode", Matchers.is("P009")));  // Use the correct product code
    }

    @Test
    public void testGetAllProducts() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        List<ProductDto> productList = new ArrayList<>();

        // Add some sample products to the list
        ProductDto product1 = new ProductDto("P001", "Test Product", "Sample product for testing", BigDecimal.valueOf(100), 10, 5, LocalDateTime.now());
        productList.add(product1);

        ProductDto product2 = new ProductDto("P002", "Test Product", "Sample product for testing", BigDecimal.valueOf(100), 10, 5, LocalDateTime.now());
        productList.add(product2);

        when(productService.getAllProducts()).thenReturn(productList);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/products")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].productCode", Matchers.is("P001")));
    }

    @Test
    public void testGetProductById() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        LocalDateTime expirationDate = LocalDateTime.now();
        Long productId = 1L;
        ProductDto productDto = new ProductDto("P001", "Test Product", "Sample product for testing", BigDecimal.valueOf(100), 10, 5, expirationDate);
        when(productService.getProductById(productId)).thenReturn(productDto);
        String[] parts = expirationDate.toString().split("[T\\-:]|\\.");
        List<Integer> expectedExpirationDate = new ArrayList<>();
        for (String part : parts) {
            expectedExpirationDate.add(Integer.parseInt(part));
        }

        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/{id}", productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.productCode", Matchers.is("P001")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productName", Matchers.is("Test Product")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", Matchers.is("Sample product for testing")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.unitPrice", Matchers.is(100)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantityInStock", Matchers.is(10)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantityThreshold", Matchers.is(5)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.expirationDate", Matchers.is(expectedExpirationDate)));
    }

    @Test
    public void testDeleteProductById() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        Long productId = 1L;

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/products/{id}", productId))
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
