package ma.nemo.assignment.config;
import ma.nemo.assignment.service.SupplyService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SupplyServiceTestConfig {

    @Bean
    public SupplyService supplyService() {
        return Mockito.mock(SupplyService.class);
    }
}
